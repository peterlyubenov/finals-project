﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalsProject.Services;
using FinalsProject.DataAccess.Entities;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
		private ProductService products;
		public HomeController()
		{
			

			products = new ProductService();

		}

        public ActionResult Index()
        {
			HttpContext.Session["Cars"] = 1;

			ProductViewModel model = new ProductViewModel() { Products = products.GetAll() };

			ViewBag.cartAmt = 0;
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.cartAmt = 2;
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.cartAmt = 3;
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}