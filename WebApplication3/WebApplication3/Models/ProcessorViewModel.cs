﻿namespace WebApplication3.Models
{
    public class ProcessorViewModel
    {
        public string Name { get; set; }
        public int Frequency { get; set; }
        public int Cores { get; set; }
        public int Threads { get; set; }
    }
}