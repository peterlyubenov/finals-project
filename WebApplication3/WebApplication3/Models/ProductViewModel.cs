﻿using FinalsProject.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
	public class ProductViewModel
	{
		public List<Product> Products { get; set; }
	}
}