﻿namespace WebApplication3.Models
{
    public class ComputerViewModel
    {
        public string Name { get; set; }
        public int HDDCapacity { get; set; }
        public int RamCapacity { get; set; }
        public ProcessorViewModel processor { get; set; }
    }
}