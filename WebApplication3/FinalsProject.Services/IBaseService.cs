﻿using System.Collections.Generic;

namespace FinalsProject.Services
{
	public interface IBaseService<T>
	{
		List<T> GetAll();

		T GetByID(int id);

		void Save(T item);
	}
}
