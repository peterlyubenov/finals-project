﻿using FinalsProject.DataAccess.Entities;
using FinalsProject.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.Services
{
	public class ComputerService : BaseService<Computer>
	{
		private ComputerRepository repository;

		public ComputerService()
		{
			repository = new ComputerRepository();
		}

		public new List<Computer> GetAll()
		{
			return repository.GetAll();
		}

		public new Computer GetByID(int id)
		{
			return repository.GetByID(id);
		}

		public new void Save(Computer computer)
		{
			repository.Save(computer);
		}

	}
}
