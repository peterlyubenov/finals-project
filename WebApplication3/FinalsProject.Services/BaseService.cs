﻿using System.Collections.Generic;
using FinalsProject.DataAccess.Repository;
using System;

namespace FinalsProject.Services
{
	public abstract class BaseService<T> : IBaseService<T>
	{
		
		public List<T> GetAll()
		{
			throw new NotImplementedException(); 
		}

		public T GetByID(int id)
		{
			throw new NotImplementedException();
		}

		public void Save(T item)
		{
			throw new NotImplementedException();
		}
	}
}
