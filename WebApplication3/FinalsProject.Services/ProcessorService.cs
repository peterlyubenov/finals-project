﻿using FinalsProject.DataAccess.Entities;
using FinalsProject.DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.Services
{
	public class ProcessorService : BaseService<Processor>
	{
		private ProcessorRepository repository;

		public ProcessorService()
		{
			repository = new ProcessorRepository();
		}

		public new List<Processor> GetAll()
		{
			return repository.GetAll();
		}

		public new Processor GetByID(int id)
		{
			return repository.GetByID(id);
		}

		public new void Save(Processor processor)
		{
			repository.Save(processor);
		}

	}
}
