﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalsProject.DataAccess.Entities;
using FinalsProject.DataAccess.Repository;

namespace FinalsProject.Services
{
	public class ProductService : BaseService<Product>
	{
		private ProductRepository repository;

		public ProductService()
		{
			repository = new ProductRepository();
		}

		public new List<Product> GetAll()
		{
			return repository.GetAll();
		}

		public new Product GetByID(int id)
		{
			return repository.GetByID(id);
		}

		public new void Save(Product product)
		{
			repository.Save(product);
		}
	}
}
