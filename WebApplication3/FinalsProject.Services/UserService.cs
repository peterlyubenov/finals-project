﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalsProject.DataAccess.Repository;
using FinalsProject.DataAccess.Entities;

namespace FinalsProject.Services
{
	public class UserService : BaseService<User>
	{
		private UserRepository repository;

		public UserService()
		{
			repository = new UserRepository();
		}

		public new List<User> GetAll()
		{
			return repository.GetAll();
		}

		public new User GetByID(int id)
		{
			return repository.GetByID(id);
		}

		public new void Save(User user)
		{
			repository.Save(user);
		}
	}
}
