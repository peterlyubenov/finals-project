﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.DataAccess
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    class LocalConfiguration : DbConfiguration
    {
        public LocalConfiguration()
        {
            SetDefaultConnectionFactory(new LocalDbConnectionFactory("MSSQLLocalDB"));
        }
    }
}
