namespace FinalsProject.DataAccess.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using FinalsProject.DB.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<FinalsProject.DataAccess.FinalsProjectDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FinalsProject.DataAccess.FinalsProjectDbContext context)
        {
            if (!context.Products.Any())
            {
                context.Products.AddOrUpdate(x => x.Id,
                    new Product() { Id = 1, Price = 1000, Discount = 20, ComputerId = 1 },
                    new Product() { Id = 2, Price = 700, Discount = 0, ComputerId = 2 }
                );
            }

            if(!context.Computers.Any())
            {
                context.Computers.AddOrUpdate(x => x.Id,
                    new Computer() {
                        Id = 2,
                        ComputerBrandId = 1,
                        HDDCapacity = 500,
                        MemoryCapacity = 8,
                        PSUWattage = 500,
                        MemoryType = "2xDDR3",
                        ProcessorId = 1,
                        Name = "IdeaCentre 300S-11IBR"
                    },
                    new Computer()
                    {
                        Id = 1,
                        ComputerBrandId = 2,
                        HDDCapacity = 1000,
                        MemoryCapacity = 16,
                        PSUWattage = 700,
                        MemoryType = "2xDDR4", 
                        ProcessorId = 2,
                        Name = "Aspire GX-781"
                    }

                );
            }

            if (!context.Processors.Any())
            {
                context.Processors.AddOrUpdate(x => x.Id,
                    new Processor()
                    {
                        Id = 1,
                        ProcessorBrandId = 1,
                        Cores = 4,
                        Frequency = (decimal)3.5,
                        Threads = 8,
                        Model = "Core� i3 - 7100"
                    },
                    new Processor()
                    {
                        Id = 2,
                        ProcessorBrandId = 1,
                        Cores = 4,
                        Threads = 4,
                        Frequency = (decimal)1.6,
                        Model = "Celeron J3160"
                    }
                );
            }

            if (!context.ComputerBrands.Any())
            {
                context.ComputerBrands.AddOrUpdate(x => x.Id, 
                    new ComputerBrand() { Id = 1, Brand = "Lenovo"},
                    new ComputerBrand() { Id = 2, Brand = "Acer"}
                );
            }

            if (!context.CPUBrands.Any())
            {
                context.CPUBrands.AddOrUpdate(x => x.Id,
                    new ProcessorBrand { Id = 1, Brand = "Intel" }
                );
            }
        }
    }
}
