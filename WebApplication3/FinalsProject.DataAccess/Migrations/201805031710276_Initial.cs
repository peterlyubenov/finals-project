namespace FinalsProject.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ComputerBrands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Computers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        ComputerBrandId = c.Int(nullable: false),
                        ProcessorId = c.Int(nullable: false),
                        MemoryCapacity = c.Int(nullable: false),
                        MemoryType = c.String(maxLength: 20),
                        HDDCapacity = c.Int(nullable: false),
                        PSUWattage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ComputerBrands", t => t.ComputerBrandId, cascadeDelete: true)
                .ForeignKey("dbo.Processors", t => t.ProcessorId, cascadeDelete: true)
                .Index(t => t.ComputerBrandId)
                .Index(t => t.ProcessorId);
            
            CreateTable(
                "dbo.Processors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Model = c.String(nullable: false, maxLength: 20),
                        ProcessorBrandId = c.Int(nullable: false),
                        Cores = c.Int(nullable: false),
                        Threads = c.Int(nullable: false),
                        Frequency = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProcessorBrands", t => t.ProcessorBrandId, cascadeDelete: true)
                .Index(t => t.ProcessorBrandId);
            
            CreateTable(
                "dbo.ProcessorBrands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Brand = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Int(nullable: false),
                        ProductDescription = c.String(maxLength: 1000),
                        Discount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Computers", "ProcessorId", "dbo.Processors");
            DropForeignKey("dbo.Processors", "ProcessorBrandId", "dbo.ProcessorBrands");
            DropForeignKey("dbo.Computers", "ComputerBrandId", "dbo.ComputerBrands");
            DropIndex("dbo.Processors", new[] { "ProcessorBrandId" });
            DropIndex("dbo.Computers", new[] { "ProcessorId" });
            DropIndex("dbo.Computers", new[] { "ComputerBrandId" });
            DropTable("dbo.Products");
            DropTable("dbo.ProcessorBrands");
            DropTable("dbo.Processors");
            DropTable("dbo.Computers");
            DropTable("dbo.ComputerBrands");
        }
    }
}
