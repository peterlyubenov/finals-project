﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.DataAccess.Entities
{
	public class User : BaseEntity
	{
		
		[Required, StringLength(30)]
		[Index("IX_UserNameUnique", IsUnique = true)]
		public string Username { get; set; }

		[Required, StringLength(50)]
		[Index("IX_EmailUnique", IsUnique = true), DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required, DataType(DataType.Password), StringLength(64)]
		public string Password { get; set; }
		
		[Required]
		public int RoleId { get; set; }

		[ForeignKey("RoleId")]
		public Role Role { get; set; }
	}
}
