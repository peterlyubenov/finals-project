﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinalsProject.DataAccess.Entities
{
    public class Computer : BaseEntity
    {
        public int ProcessorId { get; set; }
        [ForeignKey("ProcessorId")]
        public virtual Processor Processor { get; set; }    

        public int HardDriveCapacity { get; set; }
        public int RamCapacity { get; set; }

    }
}
