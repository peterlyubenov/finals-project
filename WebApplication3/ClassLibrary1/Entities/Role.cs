﻿using System.ComponentModel.DataAnnotations;

namespace FinalsProject.DataAccess.Entities
{
	public class Role : BaseEntity
	{
		[Required, StringLength(20)]
		public string Name { get; set; }
	}
}
