namespace FinalsProject.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Computers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        ProcessorId = c.Int(nullable: false),
                        HardDriveCapacity = c.Int(nullable: false),
                        RamCapacity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Processors", t => t.ProcessorId, cascadeDelete: true)
                .Index(t => t.ProcessorId);
            
            CreateTable(
                "dbo.Processors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Frequency = c.Int(nullable: false),
                        Cores = c.Int(nullable: false),
                        Threads = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 20),
                        Description = c.String(nullable: false, maxLength: 500),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        isAvailable = c.Int(nullable: false),
                        ComputerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Computers", t => t.ComputerId, cascadeDelete: true)
                .Index(t => t.ComputerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "ComputerId", "dbo.Computers");
            DropForeignKey("dbo.Computers", "ProcessorId", "dbo.Processors");
            DropIndex("dbo.Products", new[] { "ComputerId" });
            DropIndex("dbo.Computers", new[] { "ProcessorId" });
            DropTable("dbo.Products");
            DropTable("dbo.Processors");
            DropTable("dbo.Computers");
        }
    }
}
