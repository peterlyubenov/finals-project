﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.DB.Entities
{
    public class ProcessorBrand : BaseEntity
    {
        [Required, MaxLength(20)]
        public string Brand { get; set; }
    }
}
