﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalsProject.DB.Entities
{
    public class Product : BaseEntity
    {
        [Required]
        public int Price { get; set; }

        [MaxLength(1000)]
        public string ProductDescription { get; set; }

        [Range(0, 99)]
        public int Discount { get; set; }

        [Required]
        public int ComputerId { get; set; }

        [Required, ForeignKey("ComputerId")]
        public Computer Computer { get; set; }
    }
}
