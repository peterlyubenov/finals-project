﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FinalsProject.DB.Entities
{
    public class Computer : BaseEntity
    {
        //PC Configuration's Name
        [Required, StringLength(20)]
        public string Name { get; set; }

        //PC Configuration's Brand
        [Required]
        public int ComputerBrandId { get; set; }
        [Required, ForeignKey("ComputerBrandId")]
        public virtual ComputerBrand Brand { get; set; }


        /* 
         *  +------------------------------+
         *  |  List of Parts in the build  |
         *  +------------------------------+
         */

        //Processor
        [Required]
        public int ProcessorId { get; set; }
        [Required, ForeignKey("ProcessorId")]
        public virtual Processor Processor { get; set; }

        //Random Access Memory
        [Required]
        public int MemoryCapacity { get; set; }
        [MaxLength(20)]
        public string MemoryType { get; set; }

        //Hard Drive
        public int HDDCapacity { get; set; }
        //PSU
        public int PSUWattage { get; set; }
    }
}
