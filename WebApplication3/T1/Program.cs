﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinalsProject.DataAccess;
using System.Data.Entity.Migrations;
using FinalsProject.DataAccess.Entities;
using System.Data.Entity.Validation;

namespace T1
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new CustomDbContext();

            try
            {


                if (!context.Products.Any())
                {
                    context.Products.AddOrUpdate(x => x.Id,
                        new Product() { Id = 1, Name = "Laptop Acer Pavilio x8", ComputerId = 1, Description = "asdasdasdasdas dasdasd", isAvailable = 1 }
                    );

                    context.SaveChanges();
                }

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
